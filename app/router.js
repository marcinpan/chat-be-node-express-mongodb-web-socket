/* eslint-disable array-callback-return */
const Message = require('../models/message');
const User = require('../models/user');
const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

module.exports = (router, passport) => {

  router.route('/').get((req, res) => {
    res.sendFile('index.html', {root: path.join(__dirname, 'public')});
  });

  router.route('/messages')
    .post((req, res) => {
      const message = new Message(req.body.message);
      message.save((err) => {
        if (err) {
          res.send(err);
        }
        res.json({ message });
      });
    })
    .get((req, res) => {
      if(!Object.getOwnPropertyNames(req.query).length) {
        Message.find({ recipient: null }, (err, messages) => {
          if (err) {
            res.send(err);
          }
          res.json({ messages });
        });
      } else {
        Message.find({ $or: [
            { recipient: req.query.recipientId, author: req.query.authorId },
            { recipient: req.query.authorId, author: req.query.recipientId }
          ]}, (err, messages) => {
          if (err) {
            res.send(err);
          }
          res.json({ messages });
        });
      }
    });

  router.route('/users')
    .get((req, res) => {
      User.find((err, user) => {
        if (err) {
          res.send(err);
        }
        res.json({ user });
      });
    });

  router.route('/me')
    .post((req, res) => {
      const user = {
        email: req.body.email
      };
      User.findOne(user, (err, user) => {
        if (err) {
          res.json(err);
        }
        if(!user) {
          res.status(401).json('no user found');
        } else {
          res.json({
            id: user.id,
            email: user.email,
            username: user.username
          });
        }
      });
    });

  router.route('/signup')
    .post((req, res, next) => {
      passport.authenticate('local-signup', (err, user) => {
        if (err) {
          console.log(err);
        }
        if (!user) {
          res.json({ user });
        }
      })(req, res, next);
    });

  router.route('/token-auth')
    .post((req, res, next) => {
      passport.authenticate('local-username', (err, user) => {
        if (err) {
          throw err;
        }
        if (!user) {
          res.status(401).json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user) {
          if (user.invalidPassword) {
            res.status(401).json({ success: false, message: 'Authentication failed. Wrong password.' });
          } else {
            const payload = {};
            jwt.sign(payload, 'SECRETCODE', {
              expiresIn: '1h',
            }, (err, token) => {
              res.json({
                token: token,
                userId: user.id
              });
            });
          }
        }
      })(req, res, next);
    });

  router.route('/token-refresh')
    .post((req, res) => {
      jwt.verify(req.body.token, 'SECRETCODE', (err, decoded) => {
        if (err) {
          res.status(401).send({
            error: err,
          });
        } else {
          delete decoded.iat;
          delete decoded.exp;
          res.send({
            token: jwt.sign(decoded, 'SECRETCODE', {
              expiresIn: '1h',
            }),
          });
        }
      });
    });
};
