/* eslint-disable no-shadow */

// load all the things we need
const LocalStrategy = require('passport-local').Strategy;

// load up the user model
const User = require('../models/user');

// expose this function to our app using module.exports
module.exports = (passport) => {
  // =========================================================================
  // passport session setup ==================================================
  // =========================================================================
  // required for persistent username sessions
  // passport needs ability to serialize and unserialize users out of session

  // used to serialize the user for the session
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  // used to deserialize the user
  passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
      done(err, user);
    });
  });

  // =========================================================================
  // LOCAL SIGNUP ============================================================
  // =========================================================================
  // we are using named strategies since we have one for username and one for signup
  // by default, if there was no name, it would just be called 'local'

  passport.use('local-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true, // allows us to pass back the entire request to the callback
  }, (req, username, password, done) => {
    // asynchronous
    // User.findOne wont fire unless data is sent back
    process.nextTick(() => {
      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to username already exists
      User.findOne({'username': username}, (err, user) => {
        // debugger;
        // if there are any errors, return the error
        if (err) {
          return done(err);
        }
        // check to see if theres already a user with that email
        if (user) {
          return done(null, false);
        }
        // if there is no user with that email
        // create the user
        const newUser = new User();

        // set the user's local credentials
        newUser.username = req.body.username;
        newUser.email = req.body.email;
        newUser.password = newUser.generateHash(password);

        // save the user
        newUser.save((err) => {
          if (err) {
            throw new Error(err);
          } else {
            return done(null, newUser);
          }
        });
      });
    });
  }));

  // =========================================================================
  // LOCAL LOGIN =============================================================
  // =========================================================================
  // we are using named strategies since we have one for username and one for signup
  // by default, if there was no name, it would just be called 'local'

  passport.use('local-username', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true, // allows us to pass back the entire request to the callback
  },
  (req, username, password, done) => {
    // callback with email and password from our form
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to username already exists
    User.findOne({ 'username': username }, (err, user) => {
      // if there are any errors, return the error before anything else
      if (err) {
        return done(err);
      }

      // if no user is found, return the message
      if (!user) {
        return done(null, false); // req.flash is the way to set flashdata using connect-flash
      }
      // if the user is found but the password is wrong
      if (!user.validPassword(password)) {
        return done(null, { invalidPassword: true });
        // req.flash is the way to set flashdata using connect-flash
      }
      // all is well, return successful user
      return done(null, user);
    });
  }));
};
