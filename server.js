// server.js
const Message = require('./models/message');

// modules
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var passport = require('passport');
var mongoose = require('mongoose');
// var session = require('express-session');
var http = require('http').Server(app);
var io = require('socket.io')(3100);


// set our port
var port = process.env.PORT || 3000;

// set up mongoose, assume locally installed

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/RESTServer', {useMongoClient: true})
  .then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err));

// set the static files location for our Ember application
app.use(express.static(__dirname + '/public'));

// app.use(express.static(path.join(__dirname, 'public')));

require('./config/passport')(passport); // pass passport for configuration

//bodyParser Middleware to allow different encoding requests
app.use(morgan('dev')); // log every request to the console
// app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); // to support JSON-encoded bodies

// app.use(session({ secret: 'secret', resave: true }));
app.use(passport.initialize());
// app.use(passport.session());

//TODO: uncomment this and make this work
// app.set('superSecret', config.secret); // secret variable

//Routes API
var router = express.Router();
app.use('/api', router);
require('./app/router')(router, passport); // load our routes and pass in our app and fully configured passport

// startup our app at http://localhost:3000
http.listen(port);

io.on('connection', (socket) => {
  socket.on('global', (msg) => {
    new Message(msg).save((err) => {
      if (err) {
        console.log(err);
      }
    });
    socket.broadcast.emit('global', msg);
  });

  socket.on('private', (msg) => {
    new Message(msg).save((err) => {
      if (err) {
        console.log(err);
      }
    });
    socket.broadcast.emit(msg.recipient, msg);
  });
});

// expose app
exports = module.exports = app;
